"""Find a package for the relevant platforms.

This script executes searches for a 'package=version' spec across all
selected platforms ('subdirs' in conda speak) and attempts to format
the discovered packages in the right way for inclusion in the IGWN
Conda Distribution packages/ files.

The best way to use it is via `scripts/search.sh` (which handles selecting
the correct interpreter):

    $ ./scripts/search.sh <package> <version> -S <SCCB_ID>

where <package> gives the package name, <version> the desired version, and
<SCCB_ID> the ID (number) of the SCCB request ticket.
"""

import argparse
import os
import re
import sys
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from subprocess import (
    CalledProcessError,
    check_output,
)

import git
from conda_build.metadata import ARCH_MAP

from igwn_conda.conda import (
    CONDA,
    call as call_conda,
)
from igwn_conda.log import create_logger

LOGGER = create_logger("search")

# conda platforms to search
SUBDIRS = {
    "linux-64",
    "linux-aarch64",
    "linux-ppc64le",
    "osx-64",
    "osx-arm64",
    "win-64",
}
SUBDIRS_OS = {
    "linux": {s for s in SUBDIRS if s.startswith('linux-')},
    "osx": {s for s in SUBDIRS if s.startswith('osx-')},
    "win": {s for s in SUBDIRS if s.startswith('win-')},
}
SUBDIRS_OS["unix"] = SUBDIRS_OS["linux"] | SUBDIRS_OS["osx"]

DEFAULT_SUBDIRS = [
    "linux-64",
]

PACKAGES_DIR = (
    Path(__file__).parent.parent / "packages"
).relative_to(Path.cwd())
PACKAGES_FILE_DEFAULT = f"{PACKAGES_DIR}/{{package}}.yaml"

# existing keys _after_ which to preserve content when
# formatting new YAML files
PKG_CONFIG_KEYS = [
    "dependencies",
    "test"
]

# URL of SCCB issues to include in git commit messages
SCCB_ISSUES_URL = "https://git.ligo.org/computing/sccb/-/issues"


# -- search utilities -----------------

def _search_spec(
    package,
    version,
    build=None,
):
    """Return spec string to pass to ``conda search``.

    Examples
    --------
    >>> _search_spec("numpy", "2.0.0")
    '^numpy$=2.0.0'
    >>> _search_spec("gwpy", "3.0.9", "*_1")
    '^gwpy$=3.0.9=*_1'
    """
    return f"^{package}$={version}" + (f"={build}" if build else "")


def _is_cpython_build(package):
    """Identify python packages built against cpython (as opposed to pypy).

    Also returns `True` for packages that don't have any dependence on
    ``python_abi``.
    """
    deps = package["depends"]
    names = [x.split(" ", 1)[0] for x in deps]
    try:
        pythonabi = deps[names.index("python_abi")]
    except ValueError:  # not a python package
        return True
    # return only packages that match the appropriate python_abi regex
    return bool(re.match(r"python_abi [\d\.]+\* \*_cp\d+(m)?", pythonabi))


def _has_needs(package, needs):
    """Return `True` if this package satisfies all of the ``needs``

    ``needs`` should be a list of `re.Pattern` compiled regular expressions
    against which to match the list of dependencies in ``package['depends']``.
    """
    for need in needs or []:
        for d in package["depends"]:
            if need.search(d):  # found!
                break
        else:  # no break, so nothing found
            return False
    return True


# -- formatting -----------------------

def _build_string(packagedata):
    """Return the ``build_string`` for this package.

    Includes selectors for platform, architecture, and Python version.
    """
    build = packagedata["build"]
    subdir = packagedata["subdir"]
    selectors = []
    # get selectors for the platform
    if subdir == "noarch":
        selectors.append(_noarch_selector(packagedata))
    else:
        selectors.append(_platform_selector(packagedata["subdir"]))
    # get selectors for the Python version
    if (match := re.search(r"(\A|_)py(\d+)h", build)):
        selectors.append(f"py=={match.groups()[-1]}")
    selectors = list(filter(None, selectors))
    # construct the YAML build string
    text = str(build)
    if selectors:
        text += f"  # [{' and '.join(selectors)}]"
    return text


def _noarch_selector(packagedata):
    """Return the selector applicable to a noarch package.

    Examples
    --------
    >>> _noarch_selector({"depends": ["python", "numpy"]})
    None
    >>> _noarch_selector({"depends": ["__unix", "python", "numpy"]})
    'unix'
    """
    deps = packagedata["depends"]
    selectors = []
    for plat in SUBDIRS_OS:
        # if the virtual package for this platform is in the dependencies
        # add it as a selector
        if f"__{plat}" in deps:
            selectors.append(plat)
    if selectors:
        return " and ".join(selectors)


def _package_subdirs(packagedata):
    """Return the set of platforms applicable to a (noarch) package.

    Examples
    --------
    >>> _package_subdirs({"subdir": "linux-64", "depends": ["python"])
    {'linux-64'}
    >>> _package_subdirs({
    ...     "subdir": "noarch",
    ...     "depends": ["__unix", "python", "numpy"],
    ... })
    {'linux-64', 'linux-aarch64', 'linux-ppc64le'}
    """
    # if this isn't noarch, return just the one platform
    if packagedata["subdir"] != "noarch":
        return {packagedata["subdir"]}

    # otherwise scan the dependencies for virtual packages that restrict
    # the runtime platform
    deps = packagedata["depends"]
    subdirs = set()
    for plat, subset in SUBDIRS_OS.items():
        # if the virtual package for this platform is in the dependencies
        # add it as a selector
        if f"__{plat}" in deps:
            subdirs.update(subset)
    # if nothing matched, this noarch package is (naively) valid on all platforms
    return subdirs or set(SUBDIRS)


def _platform_selector(subdir):
    """Return the selector applicable to this platform.

    Unrecognised inputs are returned unmodified.

    Examples
    --------
    >>> _platform_selector("linux-64")
    'linux and x86_64'
    >>> _platform_selector('osx-arm64')
    'osx and arm64'
    >>> _platform_selector('win')
    'win'
    >>> _platform_selector('something else')
    'something else'
    """
    try:
        plat, arch = subdir.split("-", 1)
    except ValueError:  # unrecognised, just leave it
        return subdir
    return f"{plat} and {ARCH_MAP.get(arch, arch)}"


def _subdir_skips(included):
    """Return set of skipped subdirs to select as 'skip: true'.

    Examples
    --------
    >>> _subdir_skips({'linux-64'})
    {'linux-aarch64', 'linux-ppc64le', 'osx', 'win'}
    """
    excluded = set(SUBDIRS) - set(included)
    for (plat, subset) in SUBDIRS_OS.items():
        # if all subdirs for the platform are excluded, exclude the platform
        # instead of each subdir individually (looks nicer in YAML file)
        if subset.issubset(excluded):
            excluded -= subset
            excluded.add(plat)
    return excluded


# -- conda search ---------------------

def conda_search_subdirs(
    package,
    version,
    build,
    subdirs=DEFAULT_SUBDIRS,
    noarch=True,
    **kwargs,
):
    """Search for a given package/version/build on all subdirs.
    """
    def _search(subdir):
        """Partial function to search on a specific subdir
        """
        error = subdir in DEFAULT_SUBDIRS
        try:
            return subdir, list(conda_search(
                package,
                version,
                build,
                subdir,
                logger=LOGGER,
                log_errors=error,
                **kwargs,
            ))
        except CalledProcessError:
            if not error:
                return subdir, []
            raise

    # search for noarch first
    if noarch:  # search noarch
        LOGGER.debug("searching noarch")
        noarch_found = 0
        for package in _search("noarch")[1]:
            yield package
            noarch_found += 1
        LOGGER.debug(f"found {noarch_found} noarch packages")
        if noarch_found:  # stop here if any packages
            return

    # otherwise search the big subdirs
    LOGGER.debug(f"searching subdirs: {', '.join(subdirs)}")
    with ThreadPoolExecutor(len(subdirs)) as pool:
        for subdir, packages in pool.map(_search, subdirs):
            (LOGGER.debug if (count := len(packages)) else LOGGER.warning)(
                f"found {count} packages for {subdir}",
            )
            for package in packages:
                yield package


def conda_search(
    package,
    version,
    build,
    subdir,
    needs=None,
    conda=CONDA,
    logger=LOGGER,
    **kwargs,
):
    content = call_conda(
        "search",
        _search_spec(package, version, build),
        f"--subdir={subdir}",
        conda=conda,
        logger=logger,
        **kwargs,
    )

    # return only builds that match our criteria
    for pkg in content[package]:
        if (
            _is_cpython_build(pkg)
            and _has_needs(pkg, needs)
        ):
            yield pkg


# -- script execution -----------------

def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "package",
        help="name of package to find",
    )
    parser.add_argument(
        "version",
        help="version of package to find",
    )
    parser.add_argument(
        "-b",
        "--build",
        help="build string of package to find",
    )
    parser.add_argument(
        "-s",
        "--subdir",
        action="append",
        default=DEFAULT_SUBDIRS,
        required=not bool(DEFAULT_SUBDIRS),
        metavar="SUBDIR",
        dest="subdirs",
        help="Conda subdirs (platforms) to search",
    )
    parser.add_argument(
        "-L",
        "--linux",
        action="store_const",
        const=SUBDIRS_OS["linux"],
        default=False,
        help=(
            "Search all Linux subdirs, alias for "
            f"'-s {' -s '.join(SUBDIRS_OS['linux'])}'"
        ),
    )
    parser.add_argument(
        "-O",
        "--macos",
        action="store_const",
        const=SUBDIRS_OS["osx"],
        default=False,
        help=(
            "Search all macOS subdirs, alias for "
            f"'-s {' -s '.join(SUBDIRS_OS['osx'])}'"
        ),
    )
    parser.add_argument(
        "-W",
        "--windows",
        action="store_const",
        const=SUBDIRS_OS["win"],
        default=False,
        help=(
            "Search all Windows subdirs, alias for "
            f"'-s {' -s '.join(SUBDIRS_OS['win'])}'"
        ),
    )

    parser.add_argument(
        "-a",
        "--skip-noarch",
        action="store_true",
        default=False,
        help="Skip searching for noarch packages",
    )
    parser.add_argument(
        "-x",
        "--skip",
        action="append",
        help="Explit 'skip' selectors to include in output YAML",
    )
    parser.add_argument(
        "-n",
        "--need",
        type=re.compile,
        action="append",
        help="regex for requirement to be satisfied",
    )
    parser.add_argument(
        "-c",
        "--conda",
        default=CONDA,
        required=CONDA is None,
        type=Path,
        help="path of conda executable",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="emit debug-level logging information",
    )
    parser.add_argument(
        "-w",
        "--write",
        metavar="FILE?",
        nargs="?",
        const=PACKAGES_FILE_DEFAULT,
        help=(
            "write the YAML to FILE, or to %(const)s if -w/--write is "
            "given without a file path"
        ),
    )
    parser.add_argument(
        "-S",
        "--sccb",
        default="FIXME",
        metavar="<ID>",
        help="ID of SCCB request for this update",
    )
    parser.add_argument(
        "-C",
        "--commit",
        action="store_true",
        default=False,
        help="commit the updated file directly to the current branch",
    )
    parser.add_argument(
        "-P",
        "--push",
        metavar="REMOTE?",
        nargs="?",
        const="origin",
        help="push the commit to the given remote",
    )
    return parser


def git_commit(
    files,
    package,
    version,
    sccb=None,
    commit=True,
    logger=LOGGER,
):
    commitmsg = f"{package} {version}"
    if sccb:
        commitmsg += (os.linesep * 2).join((
            "",  # separate from first
            f"{SCCB_ISSUES_URL}/{sccb}",
        ))

    # if not asked to commit, stop early
    if not commit:
        git_cmd = f"git commit {' '.join(files)} -m {repr(commitmsg)}"
        logger.debug(f"to commit, run '{git_cmd}'")
        return

    # do the commit
    repo = git.Repo()
    repo.index.add(files)
    commit = repo.index.commit(commitmsg)
    logger.info("update committed to git index")
    return commit


def git_push(
    commit,
    logger=LOGGER,
    remote="origin",
    platforms=None,
    target_branch="testing",
):
    repo = commit.repo
    if repo.active_branch.name == target_branch:
        raise ValueError(
            "the active branch is the target branch, refusing to push "
            "directly, did you forget to create a new branch for this "
            "change?",
        )

    title, desc = commit.message.strip().split(os.linesep * 2, 1)

    # make links look nicer in an MR description by adding the '+' suffix to
    # refs, see https://git.ligo.org/help/user/markdown.html#show-item-title
    desc = re.sub(
        rf"(^|\s+)?({SCCB_ISSUES_URL}/[0-9]+(?!\+)$)", r"\1\2+",
        desc,
        flags=re.MULTILINE,
    )

    # find the right remote
    try:
        remoteobj, = (rmt for rmt in repo.remotes if rmt.name == remote)
    except IndexError:
        raise ValueError(f"failed to find remote '{remote}'")

    # construct push options to automatically create a well-formed
    # merge request
    push_options = [
        "merge_request.create",
        "merge_request.merge_when_pipeline_succeeds",
        "merge_request.remove_source_branch",
        rf"merge_request.title={title} [{target_branch}]",
        f"merge_request.description={desc}",
        f"merge_request.label=target::{target_branch}",
    ]
    for platform in (platforms or []):
        push_options.append(f"merge_request.label={platform}")

    # push
    infos = remoteobj.push(
        repo.active_branch.name,
        signed="if-asked",
        set_upstream=True,
        verbose=True,
        verify=True,
        push_option=push_options,
    )
    logger.info("Branch pushed to remote")
    logger.info("Merge request created")
    for info in infos:
        logger.debug(info.summary)
    return commit


def main(args=None):
    parser = create_parser()
    opts = parser.parse_args(args=args)
    name = opts.package
    version = opts.version
    build = opts.build

    if opts.verbose:
        LOGGER.setLevel("DEBUG")

    LOGGER.debug(f"-- searching for '{_search_spec(name, version, build)}'")

    # -- attempt to load existing YAML file for this package

    pkg_config_file = Path(
        (opts.write or PACKAGES_FILE_DEFAULT).format(package=name),
    )
    if pkg_config_file.exists():
        # read full config file
        pkg_conf_full = pkg_config_file.read_text()
        # drop everything up to the first occurance of one of the
        # chosen keys
        #    (this should drop the existing 'package' and 'skip' keys
        #     but keep existing dependencies, tests, etc.)
        pkg_conf = ["".join(re.split(
            "^({})".format("|".join(PKG_CONFIG_KEYS)),
            pkg_conf_full,
            maxsplit=1,
            flags=re.MULTILINE,
        )[1:])]
        LOGGER.debug(f"loaded existing config from {pkg_config_file}")
    else:
        LOGGER.debug(f"package config '{pkg_config_file}' not found")
        pkg_conf = [
            "dependencies:",
            "",
            "test:",
            "  XXX add some tests XXX",
        ]

    # -- execute searches

    subdirs = sorted(
        set(opts.subdirs)  # user-selected subdirs (including default)
        | (opts.linux or set())  # include all Linux subdirs
        | (opts.macos or set())  # include all macOS subdirs
        | (opts.windows or set())  # include all Windows subdirs
    )

    out = [
        "package:",
        f"  name: {name}",
        f"  version: {version}",
    ]
    # search for all packages across all subdirs
    found_subdirs = set()
    for package in conda_search_subdirs(
        name,
        version,
        build,
        subdirs,
        noarch=not opts.skip_noarch,
        needs=opts.need,
        conda=opts.conda,
    ):
        found_subdirs.update(_package_subdirs(package))
        out.append(f"  build_string: {_build_string(package)}")

    if not found_subdirs:  # nothing found
        LOGGER.critical("Found no packages matching requirements")
        return 1

    # include skip selectors to help in rendering
    skip = set(map(_platform_selector, opts.skip or []))
    skip.update(map(_platform_selector, _subdir_skips(found_subdirs)))
    if skip:
        out.append("")
    for selector in sorted(skip):
        out.append(f"skip: true  # [{selector}]")

    # write YAML output
    if opts.write:
        stream = open(pkg_config_file, "w")
    else:
        stream = sys.stdout
    LOGGER.debug(f"search complete, updating {stream.name}")
    with stream:
        print(
            os.linesep.join(out + [""] + pkg_conf).strip(),
            file=stream,
        )
    if opts.write:
        out = check_output(
            ["git", "diff", str(stream.name)],
            shell=False,
            text=True,
        )
        LOGGER.debug(os.linesep.join((f"$ git diff {stream.name}", out)))

        # commit
        commit = git_commit(
            [stream.name],
            name,
            version,
            commit=opts.commit,
            logger=LOGGER,
            sccb=opts.sccb,
        )

        # push
        if opts.push and commit is not None:
            git_push(
                commit,
                logger=LOGGER,
                remote=opts.push,
                platforms=found_subdirs,
            )


if __name__ == "__main__":
    sys.exit(main())
