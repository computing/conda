# Copyright (C) 2024 Cardiff University

"""Utilities to generate metrics for the IGWN Conda Distribution.
"""

import argparse
import os
import sys
from pathlib import Path

from igwn_conda.log import create_logger
from igwn_conda.conda import (
    CONDA,
    call as call_conda,
)

CONDA_INFO_KEYS = [
    # don't include any labels right now
]

LOGGER = create_logger("metrics")


def format_pkg_info_metric(
    package,
    environment,
    label_keys=CONDA_INFO_KEYS,
):
    metric = f"{environment}:{package['name']}"
    value = f"{package['version']}={package['build_string']}"
    help_ = (
        f"Version information for package '{package['name']}'"
        f" in environment '{environment}'"
    )
    type_ = "gauge"

    # formatting for openmetrics compliance
    suffix = f"_{type_}" if type_ in ["info"] else ""
    labels = ','.join(
        f"{key}={repr(package[key])}" for key in label_keys
    ).replace("'", '"')
    if labels:
        labels = "{" + labels + "}"

    # construct entry
    return os.linesep.join((
        f"# TYPE {metric} gauge",
        f"# HELP {metric} {help_}",
        f"{metric}{suffix}{labels} {value}",
    ))


def conda_info_as_metrics(
    name=None,
    prefix=None,
    conda=CONDA,
    logger=LOGGER,
):
    """Call out to conda info and report the package list as INFO metrics.
    """
    args = ["list"]
    if name:
        args.append(f"--name={name}")
    elif prefix:
        args.append(f"--prefix={prefix}")
        name = Path(prefix).name
    else:
        raise ValueError("must give name= or prefix=")
    listing = call_conda(
        *args,
        conda=conda,
        logger=logger,
    )
    for pkg in listing:
        yield format_pkg_info_metric(pkg, name)


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    envargs = parser.add_mutually_exclusive_group(required=True)
    envargs.add_argument(
        "-n",
        "--name",
        help="Name of conda env to list",
    )
    envargs.add_argument(
        "-p",
        "--prefix",
        help="Prefix of conda env to list",
    )
    parser.add_argument(
        "-c",
        "--conda",
        default=CONDA,
        required=CONDA is None,
        type=Path,
        help="path of conda executable",
    )
    parser.add_argument(
        "-o",
        "--output-file",
        default="stdout",
        type=Path,
        help="Path in which to write metrics",
    )
    return parser


def main(args=None):
    parser = create_parser()
    opts = parser.parse_args(args=args)
    metrics = list(conda_info_as_metrics(
        name=opts.name,
        prefix=opts.prefix,
        conda=opts.conda,
    ))
    if str(opts.output_file) == "stdout":
        stream = sys.stdout
    else:
        stream = open(opts.output_file, "w")
    with stream:
        print(os.linesep.join(metrics), file=stream)


if __name__ == "__main__":
    sys.exit(main())
