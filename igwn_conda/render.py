#!/usr/bin/env python3

"""Render the IGWN Conda environments from the package lists
"""

import argparse
import atexit
import hashlib
import logging
import os
import re
import shutil
import sys
import tempfile
from collections import defaultdict
from pathlib import Path

import git

import jinja2

import yaml

from igwn_conda import (
    log,
    utils,
)
from igwn_conda.conda import (
    call as call_conda,
)

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

# create a logger
LOGGER = log.create_logger("render")

# git repo
REPO = git.Repo()
try:
    ACTIVE_BRANCH = REPO.active_branch.name
except TypeError:
    if "CI_COMMIT_SHA" not in os.environ:
        raise
    ACTIVE_BRANCH = os.environ["CI_COMMIT_SHA"]

REMOTE_BRANCHES = [
    x.name[len(x.remote_name) + 1:] for x in REPO.remotes.origin.refs if
    x.is_valid() and not x.name.endswith('HEAD')
]

# default file paths
_BASE_PATH = Path(__file__).parent.parent.resolve()
_DEFAULT_PACKAGES_PATH = _BASE_PATH / "packages"
_CONFIG_YAML_NAME = "igwn_conda_config.yaml"
_CONFIG_YAML_PATH = _BASE_PATH / _CONFIG_YAML_NAME
_UPSTREAM_YAML_NAME = "upstream.yaml"
_UPSTREAM_YAML_PATH = _DEFAULT_PACKAGES_PATH / _UPSTREAM_YAML_NAME
try:
    DEFAULT_CONFIG_YAML = _CONFIG_YAML_PATH.resolve(strict=True)
except FileNotFoundError:
    DEFAULT_CONFIG_YAML = None
try:
    DEFAULT_UPSTREAM_YAML = _UPSTREAM_YAML_PATH.resolve(strict=True)
except FileNotFoundError:
    DEFAULT_UPSTREAM_YAML = None

# YAML files to ignore
IGNORE = {
    "template.yaml",
    _UPSTREAM_YAML_NAME,
}

# are we on windows?
WINDOWS = sys.platform == "win32"

# supported test script formats and their executables
TEST_SCRIPT_FORMATS = {
    ".pl": "perl",
    ".py": "python",
    ".sh": "bash.exe -el" if WINDOWS else "bash -e",
}

# test file templates
TEST_TEMPLATE = {
    "imports": """
#!/usr/bin/env python{{ python_version }}
# -*- coding: utf-8 -*-

import importlib

import pytest

imports = [
{% for item in imports %}
    "{{ item }}",{% endfor %}
]


@pytest.mark.parametrize("module", imports)
def test_import(module):
    importlib.import_module(module)
""".strip(),
    "commands": """
#!/usr/bin/env python{{ python_version }}
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
from pathlib import Path

import pytest

# working directory of test executor
SRC_DIR = Path.cwd()
os.environ["SRC_DIR"] = str(SRC_DIR)

commands = [
{%- for item in commands %}
    pytest.param(
        \"\"\"{{ item['cmd']|replace('"', '\\\\"') }}\"\"\",
        {%- if 'marks' in item %}
        marks=[{{ item['marks']|join(', ') }}],
        {%- endif %}
        {%- if 'id' in item %}
        id=\"\"\"{{ item['id']|replace('"', '\\\\"') }}\"\"\",
        {%- endif %}
    ),
{%- endfor %}
]


@pytest.mark.parametrize("command", commands)
def test_command(tmp_path, request, command):
    # copy script into tmp_path
    script = SRC_DIR / request.node.callspec.id
    if script.is_file():
        shutil.copyfile(script, tmp_path / script.name)
    # run the test script
    proc = subprocess.run(
        command,
        check=False,
        shell=True,
        cwd=tmp_path,
        env=os.environ,
    )
    if proc.returncode == 77:  # SKIP
        pytest.skip(f"{command!r} skipped")
    proc.check_returncode()
""".strip(),
}


def _latest_tag(default_branch="staging"):
    """Returns the name of the most recent tag
    """
    try:
        return REPO.git.describe(abbrev=0)
    except git.GitCommandError:
        try:
            return os.environ["CI_COMMIT_TAG"]
        except KeyError:
            if default_branch in REMOTE_BRANCHES:
                return default_branch
            return ACTIVE_BRANCH


def _target_branch_ref(target):
    # target should be either 'staging' or 'testing'
    if os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME") == target:
        # if this is a MR against the target, just use the current ref
        return os.getenv("CI_COMMIT_SHA")

    if target in REMOTE_BRANCHES:
        return target

    raise RuntimeError(
        "could not locate ref for {}, available branches: {}".format(
            target, ", ".join(map(str, REMOTE_BRANCHES)),
        ),
    )


def _git_checkout(ref):
    """Checkout a specific git reference
    """
    LOGGER.debug(f"$ git checkout {ref}")
    REPO.git.checkout(ref)
    try:
        active = REPO.head.reference.name
    except TypeError:
        try:
            active = REPO.git.describe()
        except git.GitCommandError:
            active = str(REPO.head.commit)
    LOGGER.debug(f"successfully checked out {active}")


def find_packages(*bases):
    """Find the YAML files for all packages
    """
    for base in bases:
        for pkgfile in base.rglob("*.yaml"):
            if pkgfile.name in IGNORE:
                continue
            yield pkgfile


def find_test_scripts(*bases):
    """Find test scripts inside the package dirs
    """
    for base in bases:
        for ext in TEST_SCRIPT_FORMATS:
            for script in base.rglob(f"*{ext}"):
                yield script


def define_environments(
        branch,
        config,
        upstream=None,
        python_version=None,
        unpin_upstream=False,
):
    # read upstream package list
    if upstream is not None:
        with open(upstream, "r") as fobj:
            upstream = fobj.read()

    environments = {}

    for pyver in config["python"]:
        pyv = ".".join(pyver.split(",", 1)[0].split(".")[:2])

        if python_version not in [None, pyv]:  # skip this version
            continue

        # construct environment name (e.g. 'igwn-py40-testing')
        py = int(pyv.replace(".", ""))
        envd = environments[pyv] = {}
        name = "{}py{}{}".format(
            config["prefix"],
            py,
            # add -{branch} suffix for everything except stable
            "" if branch == "stable" else "-{}".format(branch),
        )

        # build list of dependencies for this environment
        uplist = sorted(utils.parse_yaml(upstream, py=py)["dependencies"])
        if unpin_upstream:  # remove version pins if requested
            uplist = [x.split('=')[0] for x in uplist]
        deps = ["python={}".format(pyver)] + uplist

        # environment definition
        envd[branch] = {
            "name": name,
            "channels": config["channels"],
            "dependencies": deps,
            "tests": {"imports": [], "commands": []},
        }
    return environments


def parse_build_string(name, raw, **context):
    try:
        return jinja2.Template(raw).render(**context)
    except TypeError as exc:
        exc.args = (
            "failed to render build_string for {}: {}".format(
                name,
                str(exc),
            ),
        )
        raise


def _get_list(dict_, key):
    """Parse a YAML map option as a list, even if given as a string
    """
    val = dict_.get(key, []) or []
    if isinstance(val, str):
        return [x.strip() for x in val.split(",")]
    return val


def _parse_test_map(test, key="cmd"):
    """Parse a test command into a mappable

    Giving simple strings is fine, but structured information can
    be given to control how pytest executes the command.
    """
    if isinstance(test, str):
        test = {key: test}
    else:
        # ensure that the primary key was given
        assert isinstance(test.get(key), str), "invalid test declaration"

    # parse the marks to pass to pytest
    marks = _get_list(test, "marks")
    if marks:
        test["marks"] = [f"pytest.mark.{m.strip()}" for m in marks]

    return test


def _parse_test_command(test):
    """Format a `test/command` entry into a pytest command dict
    """
    return _parse_test_map(test, key="cmd")


def _script_call_str(script):
    """Construct a shell call for a script based on its file extension
    """
    path = script.split(None, 1)[0]
    for suffix, stub in TEST_SCRIPT_FORMATS.items():
        if path.endswith(suffix):
            return f"{stub} {script}"
    raise ValueError(f"unrecognised script {path!r}")


def _parse_test_script(test):
    """Parse a `test/script` entry into a pytest command dict
    """
    test = _parse_test_map(test, key="name")
    test["id"] = test["name"].split(None, 1)[0]  # use script name as the ID
    test["cmd"] = _script_call_str(test.pop("name"))
    return test


def _parse_pytest_test_command(test):
    """Parse a `test/pytest` entry into a pytest command dict
    """
    test = _parse_test_command(test)
    test["id"] = test['cmd']  # use what the user gave us as the ID
    test["cmd"] = _pytest_call_str(test["cmd"])
    return test


def _pytest_call_str(args):
    """Construct a pytest call
    """
    hash_ = hashlib.md5(
        args.encode("utf-8"),
        usedforsecurity=False,
    ).hexdigest()
    return " ".join((
        "python",
        "-m", "pytest",
        "--cache-clear",
        f"--junit-xml=${{SRC_DIR}}/pytest-{hash_}.xml",
        "--no-header",
        "--reruns", "1",
        "-r a",
    )) + " " + args


TEST_PARSERS = {
    "pytest": _parse_pytest_test_command,
    "commands": _parse_test_command,
    "script": _parse_test_script,  # be forgiving and allow a singular
    "scripts": _parse_test_script,
}


def add_package(envdict, pkginfo, **context):
    # get metadata
    name = pkginfo["package"]["name"]
    version = str(pkginfo["package"].get("version", "*"))
    tests = pkginfo.get("test", {}) or {}
    build = parse_build_string(
        name,
        str(pkginfo["package"].get("build_string", "*")),
        **context,
    )
    for branch in envdict:
        # add package spec to list
        envdict[branch]["dependencies"].append(
            "{}={}={}".format(
                name,
                version,
                build,
            ).rstrip("=*"),
        )

        envtests = envdict[branch]["tests"]

        # -- parse tests

        # test/imports are simple
        envtests["imports"].extend(_get_list(tests, "imports"))

        # test/{commands,pytest,scripts} are all transformed into shell calls
        envdict[branch]["tests"]["commands"].extend(
            cmd
            for key, test_parser in TEST_PARSERS.items()
            for cmd in map(test_parser, _get_list(tests, key))
        )


def _should_retry_render(out):
    """Returns `True` if a failed ``conda create`` call should be retried
    """
    return (
        # conda
        out["exception_name"] in {"CondaHTTPError"}
        # mamba
        or (
            out["exception_name"] == "RuntimeError"
            and out["error"].startswith("RuntimeError(\"Download error")
        )
    )


def render_environment(
    env,
    retry=0,
    logger=LOGGER,
):
    """Render the requested environment

    Parameters
    ----------
    env : `dict`
        the environment to render, should at least have a
        ``'dependencies'`` key that includes a list of packages
        to use

    retry : `int`, `bool`, optional
        whether to retry the rendering if it fails with an HTTP-style error
    """
    with tempfile.TemporaryDirectory() as prefix:
        args = [
            "create",
            "--dry-run",
            "--prefix", str(prefix),
            "--solver", "libmamba",
            "--yes",
        ] + list(env["dependencies"])
        out = call_conda(*args, logger=LOGGER)

    # we have a solved environment, so parse the packages that were selected
    new = env.copy()
    new["dependencies"] = sorted([
        "{name}={version}={build_string}".format(**pkg) for
        pkg in out["actions"]["LINK"]
    ])
    return new


def write_pinned(packages, path):
    """Write the listed ``packages`` to a file in the conda 'pinned' format.

    See https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-pkgs.html#preventing-packages-from-updating-pinning
    """  # noqa: E501
    pins = defaultdict(list)
    for pkg in sorted(packages):
        split = re.split("([><=!]+)", pkg, 1)
        try:
            name, op, version = map(str.strip, split)
        except ValueError:  # no version pin, ignore
            continue
        pins[name].append(op+version)
    with open(path, "w") as pinf:
        for name, specs in pins.items():
            spec = ",".join(specs).replace(" ", "")
            print(f"{name}{spec}", file=pinf)


def render(
    name,
    ref,
    package_dirs,
    opts,
):
    LOGGER.info("-- Rendering {} {}".format(name, "".ljust(30, "-")))

    # checkout git ref
    _git_checkout(ref)

    # parse configuration
    with open(opts.config_file, "r") as fobj:
        config = utils.parse_yaml(fobj.read())
    LOGGER.info("Parsed configuration")

    environments = define_environments(
        name,
        config,
        upstream=opts.upstream_file,
        python_version=opts.python,
        unpin_upstream=opts.unpin_upstream,
    )
    LOGGER.info("The following environments will be created:")
    for pyv in environments:
        for type_ in environments[pyv]:
            LOGGER.info(environments[pyv][type_]["name"])

    # -- read package list and render environments ---

    for pkgf in find_packages(*package_dirs):
        LOGGER.debug(f"parsing {pkgf.name}")
        with open(pkgf, "r") as fobj:
            pkgdef = fobj.read()
        for pyv in environments:
            py = int("".join(pyv.split(".", 2)))

            # parse YAML definitions for this version of python
            try:
                pkg = utils.parse_yaml(pkgdef, py=py)
            except Exception as exc:
                exc.args = (
                    "failed to parse {}: {}".format(pkgf, str(exc)),
                )
                raise

            # verify that we want this package for this version
            if pkg.get("skip", False):
                continue

            # double check that the same package isn't specified twice
            pkgname = pkg["package"]["name"]
            if any(x.startswith(pkgname) for x in environments[pyv][name]):
                raise ValueError("{!r} specified in {} and {}".format(
                    pkgname,
                    pkgf,
                    opts.upstream_file,
                ))

            # parse dependencies
            for env in environments[pyv].values():
                env["dependencies"].extend(
                    pkg.get("dependencies") or [],
                )

            # parse info and store package for this environment set
            add_package(
                environments[pyv],
                pkg,
                py=py,
            )

    # -- render environments and write tests --------

    envbase = Path(opts.output_dir).resolve()
    envbase.mkdir(exist_ok=True, parents=True)

    for pyv in environments:
        for envtype, env in environments[pyv].items():
            name = env["name"]
            tests = env.pop("tests")
            envdir = envbase / name
            envdir.mkdir(exist_ok=True)

            LOGGER.info("-- Finalising {}".format(name))

            env["dependencies"].sort()

            # render environment
            if opts.skip_solve:
                LOGGER.info(f"Skipped solving {name}")
            else:
                rendered = render_environment(env, retry=1)

                # write environment file
                yamlpath = envdir / "{}.yaml".format(name)
                with yamlpath.open("w") as out:
                    yaml.dump(rendered, out, default_flow_style=False)
                LOGGER.info(f"Environment file written: {yamlpath}")

            # writted pinned file
            pinned = envdir / f"{name}.txt"
            write_pinned(env["dependencies"], pinned)
            LOGGER.info(f"Pinning file written: {pinned}")

            # write tests
            for test, template in TEST_TEMPLATE.items():
                testpath = envdir / "{}-test-{}.py".format(name, test)
                with testpath.open("w") as out:
                    print(
                        jinja2.Template(template).render(
                            python_version=pyv,
                            **{test: tests[test]}
                        ),
                        file=out,
                    )
                LOGGER.info(f"{test} tests script written: {testpath}")

            # copy test scripts into environment
            for script in find_test_scripts(*package_dirs):
                shutil.copyfile(script, envdir / script.name)
                LOGGER.debug(f"copied {script.name!r} to {envdir!s}")


# -- define command line and parse --------------


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "packages",
        metavar="DIR",
        nargs="*",
        default=[str(_BASE_PATH / "packages")],
        help="path to packages dir (can be given multiple times)",
    )
    parser.add_argument(
        "-m",
        "--config-file",
        metavar=_CONFIG_YAML_NAME,
        default=DEFAULT_CONFIG_YAML,
        required=DEFAULT_CONFIG_YAML is None,
        type=Path,
        help="path to %(metavar)s",
    )
    parser.add_argument(
        "-u",
        "--upstream-file",
        metavar=_UPSTREAM_YAML_NAME,
        default=DEFAULT_UPSTREAM_YAML,
        type=Path,
        help="path to upstream package list",
    )
    parser.add_argument(
        "-p",
        "--python",
        default=None,
        help="python version to render (default: %(default)s [all])",
    )
    parser.add_argument(
        "--stable-ref",
        default=_latest_tag(),
        help="name of stable ref",
    )
    parser.add_argument(
        "--staging-ref",
        default=_target_branch_ref("staging"),
        help="name of staging branch",
    )
    parser.add_argument(
        "--testing-ref",
        default=_target_branch_ref("testing"),
        help="name of testing branch",
    )
    parser.add_argument(
        "--skip-stable",
        action="store_true",
        default=False,
        help="skip rendering stable environments",
    )
    parser.add_argument(
        "--skip-staging",
        action="store_true",
        default=False,
        help="skip rendering staging environments",
    )
    parser.add_argument(
        "--skip-testing",
        action="store_true",
        default=False,
        help="skip rendering testing environments",
    )
    parser.add_argument(
        "--skip-solve",
        action="store_true",
        default=False,
        help="parse but don't solve for the environment",
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        metavar="ENVDIR",
        default=_BASE_PATH / "environments",
        help="output directory for environments",
    )
    parser.add_argument(
        "-U",
        "--unpin-upstream",
        action="store_true",
        default=False,
        help="remove version pins from upstream",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="print verbose output",
    )

    return parser


def main(args=None):
    parser = create_parser()
    opts = parser.parse_args(args=args)

    if opts.verbose:
        LOGGER.setLevel(logging.DEBUG)

    package_dirs = set(Path(d).resolve() for d in opts.packages)

    # -- process each ref ---------------------------

    atexit.register(_git_checkout, ACTIVE_BRANCH)

    for name, (ref, skip) in {
        "stable": (opts.stable_ref, opts.skip_stable),
        "staging": (opts.staging_ref, opts.skip_staging),
        "testing": (opts.testing_ref, opts.skip_testing),
    }.items():
        if skip:
            continue
        render(name, ref, package_dirs, opts)


if __name__ == "__main__":
    sys.exit(main())
