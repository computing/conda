#!/usr/bin/env python

"""Format the current list of environments for inclusion in the docs
"""

import argparse
import os
import sys
import tempfile
from functools import cache
from operator import itemgetter
from pathlib import Path
from shutil import copyfile

import jinja2

import yaml

from tabulate import tabulate

from .conda import call as conda
from .log import create_logger

LOGGER = create_logger("docs")

ANACONDA_CHANNELS = {
    "pkgs/free",
    "pkgs/main",
}

BASEDIR = Path(__file__).resolve().parent.parent
SUBDIR = os.getenv(
    "SUBDIR",
    os.getenv("CONDA_SUBDIR", "linux-64"),
)

DOCS_URL_BASE = "https://docs.conda.io/projects/conda/en/latest/user-guide/"
DOCS_URL_YAML = (
    f"{DOCS_URL_BASE}/tasks/manage-environments.html"
    "#creating-an-environment-from-an-environment-yml-file"
)
DOCS_URL_PINNED = (
    f"{DOCS_URL_BASE}/tasks/manage-pkgs.html"
    "#preventing-packages-from-updating-pinning"
)

ENVIRONMENT_MARKDOWN_TEMPLATE = """
# {{ name }}

This page describes the `{{ name }}` environment.

{{ source }}

## Downloads

{{ downloads }}

## Create (exact) { #exact }

To create an exact copy of the `{{ name }}` environment on your system,
including all transitive dependencies, use
[`conda env create`](https://docs.conda.io/projects/conda/en/latest/commands/env/create.html "conda env create documentation"):

```shell title="Create an exact copy of <code>{{ name }}</code> using <code>conda env create</code>"
SUBDIR=${CONDA_SUBDIR:-$(conda config --show subdir | awk '{print $2}')} \\
conda env create --file {% raw %}{{ config.site_url }}{% endraw %}environments/$SUBDIR/{{ name }}.yaml # (1)!
```

1.  The complicated-looking `SUBDIR=<...>` variable assignment before the
    ` conda env` call just determines the platform you are using,
    e.g. `linux-64`.

Then activate it

```shell title="Activate the <code>{{ name }}</code> environment."
conda activate {{ name }}
```

{{ archspec }}

## Create (simple) { #simple }

To create a version of this environment on your system specifying only
the packages constrained by the IGWN Conda Distribution definition, use
[`conda create`](https://docs.conda.io/projects/conda/en/latest/commands/create.html "conda create documentation"):

```shell title="Create a simple version of <code>{{ name }}</code> using <code>conda create</code>."
SUBDIR=${CONDA_SUBDIR:-$(conda config --show subdir | awk '{print $2}')} \\
conda create --name {{ name }} --file {% raw %}{{ config.site_url }}{% endraw %}environments/$SUBDIR/{{ name }}.txt # (1)!
```

1.  The complicated-looking `SUBDIR=<...>` variable assignment before the
    ` conda env` call just determines the platform you are using,
    e.g. `linux-64`.

Then activate it

```shell title="Activate the <code>{{ name }}</code> environment."
conda activate {{ name }}
```

!!! warning "This environment may not reproduce behaviour exactly"

    Environments created using this method will, in general, have different
    packages than the canonical copies of the full IGWN Conda Distribution
    environments built into CVMFS and thus may not reproduce any behaviours
    exactly.

    To clone an exact copy of the IGWN Conda Distribution `{{ name }}`
    environment, see [_Create (exact)_](#exact) above.

## Pin an environment to this package list { #pin }

To pin a new/existing environment to constrain installed packages to match
those found in the `{{ name }} environment, without actually installing any
of these packages, use the following steps:

1.  Create the environment (if needed):

    ```shell
    conda create -n <myenv> <packages>
    ```

1.  Activate the environment

    ```shell
    conda activate <myenv>
    ```

1.  Download the simple package file and install it as the
    [`pinned`](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-pkgs.html#preventing-packages-from-updating-pinning "conda pinning documentation")
    file for the environment:

    === "Linux / macOS"

        ```shell title="Pin the active environment to the <code>{{ name }}</code> package list."
        SUBDIR=${CONDA_SUBDIR:-$(conda config --show subdir | awk '{print $2}')} \\
        curl -fsLS {% raw %}{{ config.site_url }}{% endraw %}environments/$SUBDIR/{{ name }}.txt -o ${CONDA_PREFIX}/conda-meta/pinned # (1)!
        ```

        1.  The complicated-looking `SUBDIR=<...>` variable assignment before the
            ` conda env` call just determines the platform you are using,
            e.g. `linux-64`.

    === "Windows"

        ```powershell title="Pin the active environment to the <code>{{ name }}</code> package list."
        Invoke-WebRequest -Uri {% raw %}{{ config.site_url }}{% endraw %}environments/win-64/{{ name }}.txt -OutFile ${Env:CONDA_PREFIX}\\\\conda-meta\\\\pinned
        ```

## Packages

!!! note "Package list for {{ subdir }}"

    The following package table describes the environment contents
    on **{{ subdir }}**, see [_Downloads_](#downloads) above for links to
    YAML and TXT package lists for all supported platforms.

{{ packages }}
""".strip()  # noqa: E501


def find_subdirs(path=BASEDIR / "environments"):
    """Yield subdirs supported by this distribution.
    """
    for subdir in Path(path).glob("*"):
        if (
            subdir.name.startswith(("linux", "osx", "win"))
            and subdir.is_dir()
        ):
            yield subdir.stem


def default_environment(config_file=None):
    """Parse the default-environment key from the given YAML file
    """
    with open(config_file or BASEDIR / "igwn_conda_config.yaml", "r") as file:
        return yaml.load(file, Loader=yaml.SafeLoader)['default-environment']


def find_environments(path=None):
    if path is None:
        path = BASEDIR / "environments" / SUBDIR
    versioned = sorted(Path(path).rglob("*.yaml"))
    default = default_environment()
    return {env.stem: env for env in versioned} | {
        "igwn":
            path / default / f"{default}.yaml",
        "igwn-testing":
            path / f"{default}-testing" / f"{default}-testing.yaml",
        "igwn-staging":
            path / f"{default}-staging" / f"{default}-staging.yaml",
    }


def _source_alias_admonition(name, source):
    if name == source:  # not an alias
        return ""
    return f"""
!!! note "`{name}` is an alias for `{source}`"

    The `{name}` environment is an alias for [{source}](./{source}.md).

    This alias is provided as a convenience for users not wishing to manually
    track migrations to new versions of Python.
    The source of the alias is subject to change as the IGWN Conda Distribution
    evolves.
    """.strip()


def _archspec_admonition(name):
    archspec = os.getenv("CONDA_OVERRIDE_ARCHSPEC", None)
    if archspec is None:
        return ""

    return f"""
!!! warning "`{name}` was created using `CONDA_OVERRIDE_ARCHSPEC`"

    The `{name}` environment on `{SUBDIR}` was created by overriding the
    default value of the `archspec`
    [virtual package](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-virtual.html "conda virtual package documentation").
    This is done to maximise the compatibility of the environment with
    potential user hardware.

    When creating an exact copy of `{name}` using `conda env create`, you may
    have to reproduce this override.
    This can be done by exporting the `CONDA_OVERRIDE_ARCHSSPEC` environment
    variable with the value `"{archspec}"`:

    === "Bourne shell (`sh`, `bash`, `zsh`)"

        ```shell title="Override <code>archspec</code> conda virtual package in Bourne shell or similar"
        export CONDA_OVERRIDE_ARCHSPEC="{archspec}"
        ```

    === "C shell (`csh`, `tcsh`)"

        ```csh title="Override <code>archspec</code> conda virtual package in C shell or similar"
        setenv CONDA_OVERRIDE_ARCHSPEC "{archspec}"
        ```

    === "Fish"

        ```fish title="Override <code>archspec</code> conda virtual package in Fish shell"
        set -x CONDA_OVERRIDE_ARCHSPEC="{archspec}"
        ```
    """.strip()  # noqa: E501


def render_environment(yamlpath, **kwargs):
    """Solve the environment stored in the ``yamlpath`` file.

    Returns the JSON-format output from ``conda create``
    """
    return _render(yamlpath.resolve(), **kwargs)


@cache
def _render(path, **kwargs):
    """Solve the environment by calling out to ``conda create``.
    """
    env = yaml.load(path.read_text(), Loader=yaml.SafeLoader)
    # render environment
    with tempfile.TemporaryDirectory() as tmpdir:
        args = [
            "create",
            "--dry-run",
            "--json",
            "--prefix", str(tmpdir),
            "--yes",
        ] + env["dependencies"]
        return env["name"], conda(*args, **kwargs)


def _copy_environment_files(
    name,
    canonical,
    source,
    target,
    link_relative_to,
    logger,
):
    target.mkdir(
        parents=True,
        exist_ok=True,
    )
    for ext in (".yaml", ".txt"):
        if (src := source / f"{canonical}{ext}").exists():
            tgt = target / f"{name}{ext}"
            copyfile(src, tgt)
            logger.debug(f"copied {src} -> {tgt}")
            href = tgt.relative_to(link_relative_to)
            yield f"[{href}]({href})"
        else:
            yield "-"


def write_environment(
    name,
    yamlpath,
    outdir,
    subdirs,
    default=SUBDIR,
    logger=LOGGER,
):
    outdir = Path(outdir)
    yamlpath = Path(yamlpath)
    envdir = yamlpath.parent.parent.parent

    # render environment
    canonical_name, rendered = render_environment(
        yamlpath,
        logger=logger,
    )

    if name is None:
        name = canonical_name

    # determine source
    source = _source_alias_admonition(name, canonical_name)

    # construct downloads table
    rows = []
    for subdir in subdirs:
        ymlf, txtf = _copy_environment_files(
            name,
            canonical_name,
            envdir / subdir / canonical_name,
            outdir / subdir,
            outdir,
            logger,
        )
        rows.append((subdir, ymlf, txtf))
    downloads_table = tabulate(
        rows,
        headers=(
            "Platform",
            f"Environment YAML [[docs]({DOCS_URL_YAML})]",
            f"Pins [[docs]({DOCS_URL_PINNED})]",
        ),
        tablefmt="github",
    )

    # construct packages table
    rows = []
    for pkg in sorted(rendered["actions"]["LINK"], key=itemgetter("name")):
        channel = (
            "anaconda"
            if pkg["channel"] in ANACONDA_CHANNELS
            else pkg["channel"]
        )
        pkgurl = f"https://anaconda.org/{channel}/{pkg['name']}/"
        versionurl = f"{pkgurl}files?version={pkg['version']}"
        rows.append((
            f"[{pkg['name']}]({pkgurl})",
            f"[{pkg['version']}]({versionurl})",
            f"`{pkg['build_string']}`",
            pkg['channel'],
        ))
    packages_table = tabulate(
        rows,
        headers=('Name', 'Version', 'Build', 'Channel'),
        tablefmt="github",
    )

    # write markdown
    markdown = jinja2.Template(ENVIRONMENT_MARKDOWN_TEMPLATE).render(
        subdir=SUBDIR,
        name=name,
        source=source,
        downloads=downloads_table,
        packages=packages_table,
        archspec=_archspec_admonition(name),
    )
    with open(outdir / f"{name}.md", "w") as file:
        print(markdown, file=file)
    return file.name


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-e",
        "--environments-dir",
        default=Path.cwd() / "environments",
        type=Path,
        help="Directory path containing the rendered environments",
    )
    parser.add_argument(
        "-d",
        "--default-subdir",
        default=SUBDIR,
        required=SUBDIR is None,
        help="Default subdir to render fully",
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        default=Path.cwd() / "docs" / "environments",
        type=Path,
        help="Directory in which to write markdown documentation",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="Print verbose logging information",
    )
    return parser


def main(args=None):
    parser = create_parser()
    opts = parser.parse_args(args=args)

    # find subdirs
    subdirs = sorted(find_subdirs(opts.environments_dir))
    if opts.verbose:
        LOGGER.setLevel("DEBUG")
    LOGGER.debug("Discovered the following subdirs:")
    for subdir in subdirs:
        LOGGER.debug(f"    {subdir}")

    # create environment documentation
    for name, path in find_environments(
        opts.environments_dir / opts.default_subdir,
    ).items():
        LOGGER.info(f"Processing {name}")
        mdname = write_environment(
            name,
            path,
            opts.output_dir,
            subdirs,
            default=opts.default_subdir,
            logger=LOGGER,
        )
        LOGGER.debug(f"    {mdname} written")


if __name__ == "__main__":
    sys.exit(main())
