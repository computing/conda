# Copyright (C) 2024 Cardiff University

"""Utilities for interaction with conda.
"""

import json
import os
import shlex
import subprocess
import sys
from pathlib import Path
from shutil import which

from .log import create_logger

LOGGER = create_logger("conda")

# find conda
CONDA = (
    os.environ.get("CONDA_EXE", None)
    or which("conda")
)
if CONDA is None:  # find conda beside python
    _rel_conda = Path(sys.prefix) / "condabin" / "conda"
    if _rel_conda.is_file():
        CONDA = str(_rel_conda)


def call(
    *args,
    conda=CONDA,
    return_json=True,
    logger=LOGGER,
    log_errors=True,
):
    """Call out to the conda executable and return the parsed JSON.

    Parameters
    ----------
    args
        Arguments to pass to `subprocess.run` prefixed with the path
        of the ``conda`` executable.

    conda : `str`
        The path of the conda executable to call.

    return_json : `bool`
        Whether to return the response as JSON.

    logger : `logging.Logger`
        The Logger to use when emitting debug information.

    log_errors : `bool`
        Whether to dump stdout and stderr from failed process to the logger.
        A `subprocess.CalledProcessError` is eventually raised either way.

    Returns
    -------
    response
        The parsed response from the command.
        If `return_json=True` is given`, the response will be a JSON blob,
        probably a `dict`, otherwise it should be a `str` containing the
        ``stdout`` from the process.
    """
    conda = str(conda)
    cmd = [conda] + list(args)
    if return_json and "--json" not in cmd:
        cmd.append("--json")

    cmdstr = shlex.join(cmd)
    if logger is not None:
        logger.debug(f"$ {cmdstr}")

    # run process
    proc = subprocess.run(
        cmd,
        capture_output=True,
        check=False,
        text=True,
    )

    # parse output and handle errors
    try:
        if not return_json:
            raise json.JSONDecodeError("", "", 0)
        data = json.loads(proc.stdout)
    except json.JSONDecodeError:
        data = proc.stdout
        return_json = False
    if proc.returncode:
        if return_json:
            output = data.get("message", "<empty>")
            error = data.get("error", "<empty>")
        else:
            output = data
            error = proc.stderr
        if log_errors and logger is not None:
            logger.critical("Conda command failed")
            logger.critical(f"Output: {output}")
            logger.critical(f"Error: {error}")
        raise subprocess.CalledProcessError(
            proc.returncode,
            cmdstr,
            output=output,
            stderr=error,
        )
    return data
