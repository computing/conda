#!/usr/bin/env python

"""Publish the conda environments to a given location
"""

import argparse
import logging
import os
import re
import subprocess
import sys
import warnings
from functools import cache
from pathlib import Path
from unittest import mock

import yaml

from igwn_conda.log import create_logger
from igwn_conda.conda import (
    CONDA,
    call as call_conda,
)

_ENV_NAME_REGEX = r"(.*)-(py\d+(?:\.yaml)?)"
STABLE_ENV = re.compile(fr"{_ENV_NAME_REGEX}\Z")
TESTING_ENV = re.compile(fr"{_ENV_NAME_REGEX}-(testing)\Z")
STAGING_ENV = re.compile(fr"{_ENV_NAME_REGEX}-(staging)\Z")

# configure logging
logger = create_logger("build")


# -- utilities ----------------------------------

def _env_name(filename):
    with open(filename, "r") as fobj:
        return yaml.safe_load(fobj)["name"]


@cache
def conda_base_prefix(conda=CONDA, logger=logger):
    return call_conda(
        "info",
        "--base",
        conda=conda,
        logger=logger,
    )["root_prefix"]


def _get_env_path():
    try:
        paths = os.environ["CONDA_ENVS_PATH"].split(os.pathsep)
    except KeyError:
        # use config
        conda_config = call_conda("config", "--get", "envs_dirs")
        try:
            paths = conda_config.get("get", {})["envs_dirs"]
        except KeyError:
            # get root and user dirs and sort appropriately
            baseprefix = conda_base_prefix(logger=None)
            rootenvs = Path(baseprefix) / "envs"
            userenvs = (Path("~") / ".conda").expanduser()
            if os.access(rootenvs.parent, os.W_OK):
                paths = (rootenvs, userenvs)
            else:
                paths = (userenvs, rootenvs)

    for path in map(Path, paths):
        if os.access(path, os.W_OK):
            return path
    raise RuntimeError(
        "cannot determine conda envs path",
    )


def list_envs(**kwargs):
    base = conda_base_prefix(**kwargs,)
    envs = call_conda(
        "env",
        "list",
        **kwargs,
    )["envs"]
    envdict = {}
    for path in map(Path, envs):
        name = path.name
        if name in {
            "_build_env",
            base,
        }:
            continue
        envdict[name] = path
    return envdict


def _environment_symlink(
    source,
    target,
    dry_run=False,
):
    """Symbolically link an environment to a new name in the same directory
    """
    if dry_run:  # dummy
        return

    target = Path(target)

    # if the link is already in place, return early
    if target.is_symlink() and target.samefile(source):
        return

    # move to target directory of the link
    oldcwd = os.getcwd()
    os.chdir(target.parent)

    try:
        cwd = os.getcwd()
        target = target.relative_to(cwd)
        source = Path(source).relative_to(cwd)
        # remove old link
        if target.is_symlink():
            target.unlink()
        # make new link
        target.symlink_to(source)
    finally:
        # always move back to where we started
        os.chdir(oldcwd)


# -- conda interactions -------------------------


def _name_or_prefix_args(name=None, prefix=None, required=False):
    if required and not (name or prefix):
        raise ValueError(
            "name or prefix must be given",
        )
    if name:
        return ("--name", str(name))
    if prefix:
        return ("--prefix", str(prefix))
    return ()


def remove_environment(name=None, prefix=None, **kwargs):
    args = _name_or_prefix_args(name=name, prefix=prefix, required=True)
    return call_conda(
        "env",
        "remove",
        "--quiet",
        "--yes",
        *args,
        **kwargs,
    )


def create_environment(
    filename,
    name=None,
    prefix=None,
    cvmfscatalog=False,
    **kwargs,
):
    args = _name_or_prefix_args(name=name, prefix=prefix)
    call_conda(
        "env",
        "create",
        "--file", str(filename),
        "--quiet",
        "--solver", "libmamba",
        "--yes",
        *args,
        **kwargs,
    )

    # if we environment was create successfully using a prefix
    # (i.e. we know where it is) and we were asked to,
    # create a CVMFS file catalog file, see
    # https://cvmfs.readthedocs.io/en/stable/cpt-repo.html#managing-nested-catalogs
    if prefix and cvmfscatalog:
        catalogfile = prefix / ".cvmfscatalog"
        catalogfile.touch(exist_ok=True)
        if logger := kwargs.get("logger", None):
            logger.debug(f"Created '{catalogfile}'")


def export_environment(filename, name=None, prefix=None, **kwargs):
    args = _name_or_prefix_args(name=name, prefix=prefix)
    return call_conda(
        "env",
        "export",
        "--quiet",
        "--file", str(filename),
        *args,
        return_json=False,
        **kwargs,
    )


def update_environment(filename, name=None, prefix=None, **kwargs):
    args = _name_or_prefix_args(name=name, prefix=prefix)
    return call_conda(
        "env",
        "update",
        "--solver", "libmamba",
        "--file", str(filename),
        *args,
        **kwargs,
    )


def update_or_replace_environment(
    filename,
    *args,
    cvmfscatalog=False,
    **kwargs,
):
    try:
        return update_environment(filename, *args, **kwargs)
    except subprocess.CalledProcessError:
        warnings.warn("failed to update environment")
        try:
            remove_environment(filename)
            return create_environment(
                filename,
                *args,
                cvmfscatalog=cvmfscatalog,
                **kwargs,
            )
        except subprocess.CalledProcessError:
            pass
        raise


def activate_environment(nameorprefix, check=True, **kwargs):
    activate = Path(CONDA).parent.parent / "bin" / "activate"
    cmd = ["/bin/bash", str(activate), str(nameorprefix)]
    if kwargs.get("logger"):
        kwargs.pop("logger").debug(f"$ {' '.join(cmd)}")
    return subprocess.run(cmd, check=check, shell=False, **kwargs)


# -- main ---------------------------------------

DEFAULT_ENV_PATH = _get_env_path()


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "environments_dir",
        type=Path,
        help="path to directory containing environments",
    )
    parser.add_argument(
        "-c",
        "--conda",
        default=CONDA,
        required=CONDA is None,
        help="path to conda executable",
    )
    parser.add_argument(
        "-t",
        "--tag",
        help="build the given dated tag",
    )
    parser.add_argument(
        "-e",
        "--envs-dir",
        type=Path,
        default=DEFAULT_ENV_PATH,
        required=DEFAULT_ENV_PATH is None,
        help="directory under which to create all environments",
    )
    parser.add_argument(
        "-x",
        "--cvmfs",
        action="store_true",
        default=False,
        help="create CVMFS catalogue files for each environment",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        default=False,
        help="just print what would be done, don't actually do anything",
    )
    parser.add_argument(
        "-D",
        "--default-environment",
        help="name of environment to select as the default",
    )
    parser.add_argument(
        "--skip-staging",
        action="store_true",
        default=False,
        help="skip building staging environments",
    )
    parser.add_argument(
        "--skip-testing",
        action="store_true",
        default=False,
        help="skip building testing environments",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="print verbose output",
    )
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    # find conda
    CONDA = args.conda
    logger.debug(f"CONDA: {CONDA}")

    if args.dry_run:
        # if asked for a dry-run, mock out popen to do nothing,
        # this way we test almost everything
        _patch = mock.patch("subprocess.run", returncode=0)
        mockedpopen = _patch.start().return_value
        mockedpopen.stdout = b"{\"envs\": []}"
        mockedpopen.returncode = 0

    args.envs_dir = args.envs_dir.resolve(strict=False)
    archive_dir = args.envs_dir / "archive"
    if not args.dry_run:
        args.envs_dir.mkdir(parents=True, exist_ok=True)
        args.envs_dir = args.envs_dir.resolve(strict=True)
        archive_dir.mkdir(exist_ok=True)

    # -- run

    # find environments to build
    envs = args.environments_dir.rglob("*.yaml")

    # name of default environment
    default_env = args.default_environment
    logger.debug(f"default_environment is '{default_env}'")

    # list existing environments
    existing = list_envs(conda=CONDA, logger=logger)
    logger.info(f"{len(existing)} existing environments found")
    for env in existing:
        logger.debug(f"    {env}")

    # record new/updated environments
    ours = {}
    symlinks = {}

    for env in sorted(envs):
        name = _env_name(env)

        logger.info(f"-- Processing {name: <10} ----------")

        isstable = STABLE_ENV.match(name)
        istesting = TESTING_ENV.match(name)
        isstaging = STAGING_ENV.match(name)
        match = isstable or istesting or isstaging

        # only build tagged stable environments
        if isstable and not args.tag:
            logger.debug("skipping (no --tag)")
            continue
        if isstaging and args.skip_staging:
            logger.debug(f"skipping {name} (--skip-staging)")
            continue
        if istesting and args.skip_testing:
            logger.debug(f"skipping {name} (--skip-testing)")
            continue
        if isstable:
            envname = f"{name}-{args.tag}"
            logger.debug(f"environment will be created as {envname}")
        else:
            envname = name

        prefix = args.envs_dir / envname

        # if environment exists, try and update it
        if existing.get(envname) == prefix:
            update_or_replace_environment(
                env,
                prefix=prefix,
                logger=logger,
                cvmfscatalog=args.cvmfs,
            )
            logger.debug(f"updated {prefix.name}")
        else:
            create_environment(
                env,
                prefix=prefix,
                logger=logger,
                cvmfscatalog=args.cvmfs,
            )
            logger.debug(f"created {prefix.name}")

        # activate the environment, in case that creates files
        activate_environment(prefix, logger=logger)
        logger.debug(f"activated {prefix.name}")

        ours[envname] = prefix

        # render the environment package list for the permanent archive
        if isstable:
            export_environment(
                archive_dir / f"{prefix.name}.yaml",
                prefix=prefix,
                logger=logger,
            )
            logger.debug(f"exported {prefix.name}")

        # create or update symlink for tagged stable
        if isstable:
            # e.g. gw-py27 -> gw-py27-YYYYMMDD
            stable = prefix.parent / name
            _environment_symlink(prefix, stable, dry_run=args.dry_run)
            activate_environment(stable, logger=logger)
            logger.debug(f"activated {stable.name}")
            symlinks[name] = (stable, prefix)
            logger.debug(f"linked {name} -> {envname}")

        # link default environment, default name has no python versioning
        if default_env and name.startswith(default_env):
            groups = match.groups()
            if isstable:
                default = args.envs_dir / groups[0]
                target = stable
            else:
                default = args.envs_dir / f"{groups[0]}-{groups[2]}"
                target = prefix
            _environment_symlink(prefix, default, dry_run=args.dry_run)
            activate_environment(default, logger=logger)
            logger.debug(f"activated {default.name}")
            symlinks[default.name] = (default, target)
            logger.debug(f"linked {default.name} -> {target.name}")

        logger.debug("complete")

    logger.info("-- All environments created/updated")
    logger.debug("The following environments were created/updated:")
    for name, prefix in ours.items():
        logger.debug(f"{name}: {prefix}")
    if not ours:
        logger.debug("[EMPTY]")
    logger.debug("The following symbolic links were created:")
    for name, (link, target) in symlinks.items():
        logger.debug(f"{name}: {link} -> {target}")
    if not symlinks:
        logger.debug("[EMPTY]")


if __name__ == "__main__":
    sys.exit(main())
