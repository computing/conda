# Creating a new stable environment release

This document describes how to create a new stable release of the
IGWN Conda Distribution.

## Create a new release {: #create }

All you need to do is
[manually run](https://git.ligo.org/help/ci/jobs/job_control.html#run-a-manual-job)
the `create release` job from the
[most-recently completed CI pipeline for the `staging` branch](https://git.ligo.org/computing/conda/-/pipelines?page=1&scope=all&ref=staging).

## What does the `create release` CI job do? {: #description }

The `create release` CI job does the following

1.  Check for a (meaningfull) diff between the previous tag,
    and the latest commit on the staging (`staging`) branch.

2.  Generates a tag name based on today's date as per _CalVer_:

    [![Version format](https://img.shields.io/badge/calver-YY0M0D.MICRO-22bfda.svg)](https://calver.org/)

    If a tag already exists for today, a 'micro' version number is appended as a suffix, e.g:

    ```text
    20190101
    20190101.1
    20190102
    ```

3.  Pushes the new tag to the upstream project,
    see <https://git.ligo.org/computing/conda/-/tags>.

This new tag will then trigger a new pipeline which will

1.  Create new dated, stable environments.

2.  Create a new [release](https://git.ligo.org/computing/conda/-/releases)
    that describes the new environments.
