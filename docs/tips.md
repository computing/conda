---
title: Advanced usage
---

# Advanced usage tips and tricks

## Customising Conda behaviour

The behaviour of `conda` can be controlled by adding/removing options in
the conda configuration file, `.condarc`.o

For details on how to customise conda behaviour, see

<https://docs.conda.io/projects/conda/en/latest/user-guide/configuration/use-condarc.html>

## Managing environments

### Cloning an environment {: #clone }

The pre-built environments in CVMFS are read-only, and so cannot be modified
by users.
You can clone an environment as follows:

```shell
conda create --name <target> --clone <source>
```

e.g.:

```shell
conda create --name myigwn-py310 --clone igwn-py310
```

This should create an exact copy of an existing environment in your local directory
(normally `~/.conda`) that you can modify and upgrade freely.

## Using Conda environments

### Using Conda environments with HTCondor {: #htcondor }

The IGWN Conda Distributions in CVMFS can be used easily in HTCondor workflows.
For the most up-to-date instructions please see
[The IGWN Computing Guide](https://computing.docs.ligo.org/guide/htcondor/conda/).

### Using graphics libraries on Linux {: #graphics }

Low-latency graphics libraries (e.g. implementations of OpenGL) are not
distributed as conda packages on Linux, and as such these are not included
in the IGWN Conda Distribution environments.

!!! warning "Install graphics libraries separately"

    If your workflow requires rendering graphics, you are expected to
    install these low-level libraries yourself, or ask your friendly
    systems adminstrator to do that for you.

With most systems this just requires installing `libGL.so`, but more
graphics-intensive workflows will likely require other libraries.

!!! info "macOS comes batteries-included"

    macOS (at least macOS X) seems to come with the relevant graphics
    libraries pre-loaded.

### Using LaTeX

The IGWN Conda Distribution does not provide (La)TeX, and in general there
is not a usable TeX distribution available from conda-forge or Anaconda.

If you wish to use LaTeX alongside a Conda environment, you should install
TeX using your system package manager, see below for details for
some common operating systems:

!!! warning "Should be good enough for matplotlib"

    The following examples should get you far enough to use TeX with the
    Matplotlib Python library. If you find that other packages are needed,
    please [open a Bug
    ticket](https://git.ligo.org/computing/conda/issues/new?issuable_template=Bug).

#### Macports

```shell
port install \
    texlive \
    dvipng
```

#### Debian/Ubuntu

```shell
apt-get install \
    dvipng \
    texlive-latex-base \
    texlive-latex-extra
```

#### RHEL/Centos/Fedora

```shell
yum install \
    texlive-dvipng-bin \
    texlive-latex-bin-bin \
    texlive-type1cm \
    texlive-collection-fontsrecommended
```
