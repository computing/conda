# vim: set ft=dockerfile :
# Build IGWN Conda distribution using Docker
#
# Copyright (c) 2019 Duncan Macleod <duncan.macleod@ligo.org>
# This file is licensed under the terms of the MIT license,
# see LICENSE for full terms.
#
# This Dockerfile creates a container that should not
# be used for anything, it exists purely to provide a pristine
# build environment in which to build out the IGWN Conda environments.
#
# Conda requires that the environments are created using the production
# runtime path, which is
#
# /cvmfs/software.igwn.org/conda
#
# so we build them using `docker build` then unpack them using
# `COPY --from=builder ${INSTALL_PATH} /` so as to upload them
# to the CVMFS repo host and publish them.

# -- step 1: build the environments in their production location --------------

FROM debian:stable AS builder

ARG INSTALL_PATH=/cvmfs/software.igwn.org/conda/
ARG SUBDIR=linux-64
ARG ARCHSPEC=haswell
ARG BUILD_OPTS=""

ENV CONDA_OVERRIDE_ARCHSPEC="${ARCHSPEC}"
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH=${INSTALL_PATH}/condabin:${PATH}

RUN apt-get update --quiet --fix-missing && \
    apt-get install --quiet --quiet --yes \
      "bash" \
      "curl" \
      "libglib2.0-0" \
      "yq"

# install conda distribution and configure the base environment
COPY igwn_conda_config.yaml /tmp/
RUN mkdir -p $(dirname ${INSTALL_PATH}) && \
    curl --location --silent \
      https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh \
      --output /root/miniforge.sh && \
    /bin/bash /root/miniforge.sh -b -p ${INSTALL_PATH} && \
    rm -f /root/miniforge.sh && \
    conda config --system --append channels igwn && \
    conda config --system --set solver libmamba && \
    conda install --yes --satisfied-skip-solve $(yq -r '."base-packages"[]' /tmp/igwn_conda_config.yaml) && \
    conda info --all && \
    conda list --name base && \
    conda clean -afy && \
    touch -a $(conda info --base)/.cvmfscatalog

# copy rendered environments
COPY environments/${SUBDIR} /tmp/environments
COPY igwn_conda /tmp/lib/igwn_conda
RUN DEFAULT_ENVIRONMENT=$(yq -r '."default-environment"' /tmp/igwn_conda_config.yaml) && \
    PYTHONPATH="/tmp/lib" \
    ${INSTALL_PATH}/bin/python \
      -m igwn_conda.build \
      "/tmp/environments" \
      --conda "${INSTALL_PATH}/condabin/conda" \
      --default-environment "${DEFAULT_ENVIRONMENT}" \
      --envs-dir "${INSTALL_PATH}/envs" \
      --cvmfs \
      --verbose \
      ${BUILD_OPTS} && \
    conda clean -afy

# -- step 2: unpack them into / to act as a tarball ---------------------------

FROM scratch
ARG INSTALL_PATH=/cvmfs/software.igwn.org/conda/
COPY --from=builder ${INSTALL_PATH} /
