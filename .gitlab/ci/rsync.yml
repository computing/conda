# -- IGWN Conda Distribution rsync job construction

spec:
  inputs:
    stage:
      default: "publish"
      type: string
    job_name:
      default: "publish"
      type: string
    deployment:
      default: "$CI_MERGE_REQUEST_TARGET_BRANCH"
      type: string
    deployment_tier:
      default: "$CI_MERGE_REQUEST_TARGET_BRANCH"
      type: string
    job_name_suffix:
      default: ""
      type: string
    # by default publish all environments
    rsync_opts:
      default: ""
      type: string
    rsync_source_path:
      default: "envs/"
      type: string
    rsync_target_path:
      default: "envs/"
      type: string

---

include:
  - component: git.ligo.org/computing/gitlab/components/debian/base@~latest

.cvmfs_server:
  # start a new publish operation
  transaction:
    - |
      if [ "${CI_PIPELINE_SOURCE}" != "merge_request_event" ]; then
      ssh -T ${CVMFS_USER}@${CVMFS_REPO_HOST} "bash -ex" <<EOF
      # this sets RW permissions on the CVMFS_REPO path and locks it;
      sudo -u repo.software cvmfs_server transaction -t 300 ${CVMFS_REPO}
      # force permissions on the repo base directory again
      # see https://git.ligo.org/computing/helpdesk/-/issues/4051#note_726376
      sudo -u repo.software chmod go+rx "/cvmfs/${CVMFS_REPO}"
      EOF
      fi

  # finish the publish operation
  publish:
    # this completes the transation, sets RO permissions, and unlocks
    # (fall back to 'abort' if that fails)
    - |
      if [ "${CI_PIPELINE_SOURCE}" != "merge_request_event" ]; then
      ssh -T ${CVMFS_USER}@${CVMFS_REPO_HOST} "bash -ex" <<EOF
      sudo -u repo.software cvmfs_server publish ${CVMFS_REPO} || \
      { sudo -u repo.software cvmfs_server abort -f ${CVMFS_REPO}; exit 1; }
      EOF
      fi

  # abort the publish operation
  abort:
    - ssh -T ${CVMFS_USER}@${CVMFS_REPO_HOST}
      sudo -u repo.software cvmfs_server abort -f ${CVMFS_REPO}

  # handle which operation based on the exit code
  publish-or-abort:
    # if the rsync command wrote an exit-code of 0 then 'publish', otherwise 'abort'
    - if [ "${exit_code:=$?}" -eq 0 ]; then
        cvmfsop="publish";
        echo -e "\x1B[92mRsync succeeded, executing cvmfs_server ${cvmfsop}\x1B[0m";
      else
        cvmfsop="abort -f";
        echo -e "\x1B[91mRsync failed, executing cvmfs_server ${cvmfsop}\x1B[0m";
      fi
    - |
      if [ "${CI_PIPELINE_SOURCE}" != "merge_request_event" ]; then
      ssh -T ${CVMFS_USER}@${CVMFS_REPO_HOST} "bash -ex" << EOF
      sudo -u repo.software cvmfs_server ${cvmfsop} ${CVMFS_REPO} || \
      { sudo -u repo.software cvmfs_server abort -f ${CVMFS_REPO}; exit 1; }
      EOF
      fi
    # propagate the exit code
    - exit ${exit_code}

$[[ inputs.job_name ]]_$[[ inputs.deployment | expand_vars ]]$[[ inputs.job_name_suffix ]]:
  extends: .debian_base
  stage: $[[ inputs.stage ]]
  tags:
    # we need to use docker
    - executor-docker
  interruptible: false
  resource_group: cvmfs-rsync
  environment:
    name: $[[ inputs.deployment | expand_vars ]]
    deployment_tier: $[[ inputs.deployment_tier | expand_vars ]]
  dependencies:
    - build_$[[ inputs.deployment | expand_vars ]]
  variables:
    CVMFS_USER: "cvmfs.conda"
    SOURCE_DIR: "$CI_PROJECT_DIR/_build"
    CVMFS_REPO_HOST: "cvmfs-software.ligo.caltech.edu"
    GIT_STRATEGY: none
  before_script:
    - !reference [.debian_base, before_script]
    # install software
    - apt-get -y -q install
        coreutils
        docker.io
        gzip
        openssh-client
        rsync
        tar
    # configure SSH client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - ssh-add <(echo "${CVMFS_SSH_PRIVATE_KEY}" | base64 -d)
    - if [[ "${CI_RUNNER_TAGS}" == *"cit"* ]]; then export CVMFS_REPO_HOST="${CVMFS_REPO_HOST/ligo.caltech.edu/ldas.cit}"; fi
    - echo -e "Host ${CVMFS_REPO_HOST}\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
    # authenticate with local container registry
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    # create, then export the container to unpack the environments
    - mkdir -p ${SOURCE_DIR}
    - DOCKER_NAME="tmp${RANDOM}"
    - docker pull "${DOCKER_TAG:=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME}"
    - docker create --name "${DOCKER_NAME}" "${DOCKER_TAG}" /bin/python
    - docker export ${DOCKER_NAME} | tar -x -C ${SOURCE_DIR}
    - docker rm --force "${DOCKER_NAME}"
    - docker rmi "${DOCKER_TAG}"
    # sanity check things locally first
    - head -n 1 ${SOURCE_DIR}/condabin/conda
    - ${SOURCE_DIR}/bin/python ${SOURCE_DIR}/condabin/conda info --all
  script:
    # start a new transaction (except in MR pipelines)
    - !reference [.cvmfs_server, transaction]
    # sync the environments
    - rsync
          --archive
          --itemize-changes
          --filter='Pp .cvmfscatalog' --filter='Pp .cvmfsautocatalog'
          --log-file="$CI_PROJECT_DIR/rsync.log"
          --rsh=ssh
          $[[ inputs.rsync_opts ]]
          ${SOURCE_DIR}/$[[ inputs.rsync_source_path | expand_vars ]]
          ${CVMFS_USER}@${CVMFS_REPO_HOST}:${CVMFS_PATH}/$[[ inputs.rsync_target_path | expand_vars ]]
      1>/dev/null || exit_code=$?
    # sync the base environment
    - if [ "${exit_code:-0}" -eq 0 ]; then
      rsync
          --archive
          --delete
          --itemize-changes
          --filter='Pp .cvmfscatalog' --filter='Pp .cvmfsautocatalog'
          --log-file="$CI_PROJECT_DIR/rsync-base.log"
          --rsh=ssh
          $[[ inputs.rsync_opts ]]
          --exclude envs
          ${SOURCE_DIR}/
          ${CVMFS_USER}@${CVMFS_REPO_HOST}:${CVMFS_PATH}/
      1>/dev/null || exit_code=$?
      ; fi
    # publish the new files, or abort (except in MR pipelines)
    - !reference [.cvmfs_server, publish-or-abort]
  artifacts:
    paths:
      - rsync*.log
    when: always
