<!--
Please read this!

Before opening a new issue, make sure to search for keywords in the issues filtered by the `bug` label

https://git.ligo.org/computing/conda/issues?label_name%5B%5D=bug

and verify the issue you're about to submit isn't a duplicate.
-->
### Summary

<!-- (Summarize the bug encountered concisely) -->

### Steps to reproduce

<!-- (How one can reproduce the issue - this is very important) -->

### Environment

<details>
<summary>Environment (<code>conda list</code>):</summary>

```
$ conda list

```
</details>

<br/>
<details>
<summary>Details about  <code>conda</code> and system ( <code>conda info</code> ):</summary>

```
$ conda info

```
</details>

### What is the current *bug* behavior?

<!-- (What actually happens) -->

### What is the expected *correct* behavior?

<!-- (What you should see instead) -->

### Possible fixes

<!-- (If you can, link to the line of code that might be responsible for the problem) -->
