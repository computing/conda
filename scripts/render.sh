#!/bin/bash
#
# Utility script to render the current branch of the IGWN Conda Distribution
# for the given 'target' (defaults to 'testing')
#

set -e

SUBDIR=${CONDA_SUBDIR:-$(conda config --show subdir | awk '{print $2}')}

TARGET=$1
# if first argument is just an option for the renderer, target testing
if [[ "$TARGET" == "-"* ]]; then
  TARGET="testing"
else
  shift 1 || TARGET="testing"
fi

BRANCH=$(git branch --show-current)
echo $TARGET

if [ "${TARGET}" = "testing" ]; then
  ARGS="--skip-stable --skip-staging --testing-ref ${BRANCH}"
elif [ "${TARGET}" = "staging" ]; then
  ARGS="--skip-stable --skip-testing --staging-ref ${BRANCH}"
elif [ "${TARGET}" = "stable" ]; then
  ARGS="--skip-staging --skip-testing --stable-ref ${BRANCH}"
fi

set -x

${CONDA_EXE:-conda} run \
  --name base \
  --no-capture-output \
python -m igwn_conda.render \
  --output-dir ./environments/${SUBDIR}/ \
  -vv \
  ${ARGS} \
  "$@"
