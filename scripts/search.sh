#!/bin/bash
set -ex
${CONDA_EXE:-conda} run \
  --name base \
  --no-capture-output \
python -m igwn_conda.search "$@"
