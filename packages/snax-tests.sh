#!/bin/bash
#
# IGWN Conda Distribution tests for snax
#

# -- configurables

# name of package (according to Python metadata)
PKG_NAME="snax"

# path of test directory
TEST_PATH="tests/"

# other paths from tarball needed by tests (include leading */ for each entry)
EXTRA_PATHS=""

# test command to run
TEST_CMD="python -m pytest -ra --cache-clear --no-header"

# -- versioning

# get package version from metadata
VERSION=$(python -c "
from importlib.metadata import version
print(version('${PKG_NAME}'))
")

# -- download and run tests

# download and unpackage the tarball
URL="https://software.igwn.org/sources/source/${PKG_NAME}-${VERSION}.tar.gz"
curl -Ls ${URL} | \
tar \
  -x \
  -z \
  -f - \
  --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") \
  "*/${TESTS_DIR}" \
  ${EXTRA_PATHS} \
|| {
  echo "download failed, skipping..." 1>&2;
  exit 77;
}

# run tests
${TEST_CMD} ${TEST_PATH} "$@"
