#!/bin/bash
#
# IGWN Conda Distribution tests for PyGWB
#

# test scripts

pygwb_combine --help
pygwb_create_isotropic_workflow --help
pygwb_dag --help
pygwb_html --help
pygwb_pe --help
pygwb_pipe --help
pygwb_simulate --help
pygwb_stats --help
