#!/bin/bash
#
# IGWN Conda Distribution tests for parallel_bilby
#

# Parallel-bilby can't really be tested in CI using a
# single-core restriction (set for all IGWN Conda tests).
# So here we just sanity check some of the modules.

# check imports
python3 -c "
import parallel_bilby
import parallel_bilby.analysis
import parallel_bilby.generation
import parallel_bilby.parser
import parallel_bilby.schwimmbad_fast
import parallel_bilby.slurm
import parallel_bilby.utils
"

# check entry points
parallel_bilby_generation --help
parallel_bilby_analysis --help
