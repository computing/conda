#!/bin/bash
#
# IGWN Conda Distribution tests for LALMetaIO
#

# liblalmetaio
pkg-config --print-errors --libs lalmetaio

# python-lalmetaio
python -c "
import lalmetaio
"

# lalmetaio
lalmetaio_version
