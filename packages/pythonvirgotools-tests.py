#!/usr/bin/env python
#
# IGWN Conda Distribution tests for PythonVirgoTools
#

import virgotools as vt
import pytest
import os
import subprocess


@pytest.fixture
def sample_data(tmp_path):
    curdir = os.getcwd()
    os.chdir(tmp_path)
    try:
        subprocess.check_call(["framecpp_sample"],shell=True)
        yield tmp_path / "Z-ilwd_test_frame-600000000-1.gwf"
    finally:
        os.chdir(curdir)

def test_frame_lib(sample_data,tmp_path):

    with vt.FrameFile(str(sample_data)) as f:

        #Test getting the first frame and its gps time
        with f.first_frame() as frame:
            assert frame.gap == -1
            assert frame.gps == 600000000
            assert frame.dt == 1

            #Iterat through teh adc channels
            it = frame.iter_adc()
            adc = next(it)
            chan = adc.contents.name.data  # FIXME str() gives an error, might be a bug in ctypes wrapper

            #Get the chanel data
            x = frame.get_adc_channel(chan)

            for adc in it:  # iterate until end
                pass
            chan = adc.contents.name.data


if __name__ == "__main__":
    import sys
    import pytest
    sys.exit(pytest.main(args=[__file__] + sys.argv[1:]))
