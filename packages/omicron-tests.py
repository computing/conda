#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# IGWN Conda Distribution tests for Omicron
#

"""Download GWOSC data for GW150914 and run Omicron over them
"""

import os
import subprocess
import sys
import tempfile
import warnings
from getpass import getuser
from pathlib import Path
from shutil import which

from gwpy.io.cache import write_cache
from gwpy.timeseries import TimeSeries

OMICRON = which("omicron")
OMICRON_PARAMETERS = """
OUTPUT     DIRECTORY        {outdir}
OUTPUT     VERBOSITY        1
OUTPUT     NTRIGGERMAX      10000000.0
OUTPUT     PRODUCTS         triggers
OUTPUT     FORMAT           root hdf5

DATA       SAMPLEFREQUENCY  4096
DATA       CHANNELS         {channel}
DATA       FFL              {outdir}/data.ffl

PARAMETER  CLUSTERING       TIME
PARAMETER  TRIGGERRATEMAX   100000
PARAMETER  FFTPLAN          FFTW_ESTIMATE
PARAMETER  QRANGE           3.3166 100
PARAMETER  FREQUENCYRANGE   16.0 2048.0
PARAMETER  MISMATCHMAX      0.3
PARAMETER  SNRTHRESHOLD     5
PARAMETER  PSDLENGTH        124
PARAMETER  TIMING           32 4
""".strip()
ROOTLS = which("rootls")

# get some GWOSC data
GW150914 = 1126259462
DURATION = 64
START = GW150914 - int(DURATION/2.)
END = START + DURATION
OBSERVATORY = "L1"


def get_data(observatory, start, end):
    try:
        data = TimeSeries.fetch_open_data(
           observatory,
           start,
           end,
           sample_rate=4096,
           cache=True,
           verbose=True,
        )
    except Exception as exc:
        warnings.warn("caught {}: {}".format(type(exc).__name__, str(exc)))
        sys.exit(77)
    data.name = "L1:GWOSC-STRAIN"
    return data


def _print_file(filename):
    print("{} contents:".format(filename))
    print("----------")
    with open(filename, "r") as file:
        print(file.read(), end="")
    print("----------")


def write_data(data, outdir):
    outdir = Path(outdir)
    gwf = str(outdir / "{}-DATA-{}-{}.gwf".format(
        OBSERVATORY,
        START,
        DURATION,
    ))
    data.write(gwf, format="gwf")
    cache = outdir / "data.ffl"
    with open(cache, "w") as cachef:
        write_cache([gwf], cachef, format="ffl")
    _print_file(cache)
    return cache


def write_parameters(outdir):
    outdir = Path(outdir)
    parfile = outdir / "parameters.txt"
    with open(parfile, "w") as parf:
        print(
            OMICRON_PARAMETERS.format(
                outdir=str(outdir),
                channel="{}:GWOSC-STRAIN".format(OBSERVATORY),
            ),
            file=parf,
        )
    _print_file(parfile)
    return parfile


def _check_call(cmd, *args, **kwargs):
    cmd = list(map(str, cmd))
    print("+ {}".format(" ".join(cmd)))
    return subprocess.check_call(cmd, *args, **kwargs)


def run_omicron(start, end, parameters):
    env = os.environ
    env.setdefault("USER", getuser())
    return _check_call(
        (
            OMICRON,
            start,
            end,
            parameters,
            "strict",
        ),
        env=env,
    )


def rootls(file):
    return _check_call((
        ROOTLS,
        "--treeListing",
        file,
    ))


if __name__ == "__main__":
    # get data
    print("Getting data...")
    data = get_data(OBSERVATORY, START, END)
    print("Data:")
    print("----------")
    print(data)
    print("----------")
    # run everything in a temporary directory
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        # write data to GWF file and cache
        cache = write_data(data, tmpdir)
        # generate parameters
        parfile = write_parameters(tmpdir)
        # run omicron
        print("Running omicron...")
        run_omicron(START, END, parfile)
        # sanity check the output
        rootls(
            tmpdir
            / "L1:GWOSC-STRAIN"
            / "L1-GWOSC_STRAIN_OMICRON-*.root"
        )
