#!/bin/bash
#
# IGWN Conda Distribution tests for ldas-tools-frameAPI
#

# create some fake data
framecpp_sample

# check that get the right analysis-ready segments
ldas_analysis_ready \
  --frame-file-pattern "*.gwf" \
  --ifo Z0 \
  --analysis-ready-channel RAMPED_INT_8U_1 \
;
