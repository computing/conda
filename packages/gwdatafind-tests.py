# -*- coding: utf-8 -*-
# Copyright 2022 Cardiff University

"""IGWN Conda Distribution integration tests for the GWDataFind
client Python package.
"""

from functools import wraps
from pathlib import Path

import gwdatafind

from igwn_auth_utils import (
    IgwnAuthError,
    find_x509_credentials,
    find_scitoken,
)

import pytest

from requests import exceptions

GWDATAFIND_SERVER = "https://datafind.ligo.org"
GWOSC_DATAFIND_SERVER = "https://datafind.gwosc.org"


def have_creds():
    """Find a token or X.509 that we can use to talk to a restricted
    GWDataFind instance.

    Returns `True`, or `False`, shouldn't ever raise an exception.
    """
    try:
        find_scitoken(
            audience=GWDATAFIND_SERVER,
            scope="gwdatafind.read",
            skip_errors=True,
        )
    except IgwnAuthError:
        pass
    else:
        return True
    try:
        find_x509_credentials()
    except IgwnAuthError:
        return False
    return True


SKIP_X509 = pytest.mark.xfail(not have_creds(), reason="needs auth credential")


def pytest_skip_network_error(func):
    """Decorator to skip a pytest function on certain network errors.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (  # exceptions we care about
            exceptions.SSLError,
        ):
            raise
        except exceptions.RequestException as exc:
            if isinstance(exc, exceptions.HTTPError) and exc.response.status_code == 401:
                # auth fail
                raise
            pytest.skip(str(exc))

    return wrapper


@pytest_skip_network_error
@pytest.mark.parametrize("host", (
    GWOSC_DATAFIND_SERVER,
    pytest.param(GWDATAFIND_SERVER, marks=SKIP_X509),
))
def test_ping(host):
    """Test that a basic ping of the host works.
    """
    gwdatafind.ping(host)


@pytest_skip_network_error
@pytest.mark.parametrize(("host", "typ"), [
    (GWOSC_DATAFIND_SERVER, "H1_GWOSC_O3a_4KHZ_R1"),
    pytest.param(GWDATAFIND_SERVER, "H1_HOFT_C00", marks=SKIP_X509),
])
def test_find_types(host, typ):
    """Check that we can query for types and that we get what we expect.
    """
    types = gwdatafind.find_types(host=host)
    assert typ in types


@pytest_skip_network_error
def test_latest():
    """Test that the latest GWF for an old dataset is what it should be.
    """
    latest, = gwdatafind.find_latest(
        "H",
        "H1_GWOSC_O2_4KHZ_R1",
        host=GWOSC_DATAFIND_SERVER,
    )
    assert Path(latest).name == "H-H1_GWOSC_O2_4KHZ_R1-1187733504-4096.gwf"


if __name__ == "__main__":
    import sys
    args = sys.argv[1:] or ["-ra", "-v"]
    sys.exit(pytest.main(args=[__file__] + args))
