#!/bin/bash
#
# IGWN Conda Distribution tests for distromax
#

# download the github tag tarball which includes tests/ and examples/
VERSION=$(python -c "import distromax; print(distromax.__version__.lstrip('v'))")
URL="https://github.com/Rodrigo-Tenorio/distromax/archive/refs/tags/v${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") \
	"*/tests" \
	"*/examples" \
|| {
	echo "download failed, skipping..." 1>&2;
	exit 77;
}

python -m pytest -ra --cache-clear --no-header tests/test.py
